# Hetzner

cloud-config files to bootstrap VPS

With this file, you can use it to configure an instance at boot

It creates an st user, choose your own, also use your public key

It secures a bit SSH Server

It configures EPEL repo, to be able to install Ansible, for example

And of course, upgrades everything and reboot.

I also copied a YAML validator from another user, so in case you run into trouble, at least you can discard YAML syntaxis issues

Enjoy!
